#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import logging
import webbrowser
from .exceptions import ObjectNotFound, BitBucketAPIError, CommandError
from .core import get_bitbucket_remotes, parse_to_object

logger = logging.Logger(__file__)


def issues(name=None, params=None):
    """Returns a list of issues of a BitBucket repository.

    Raises ``ObjectNotFound`` if the given name does not match any known
    BitBucket remotes.

    :param name: The remote name pointing to a BitBucket repository. If ommited
        or given ``None``, issues from all BitBucket repositories will be
        returned.

    :param params: Extra parameters to query the BitBucket API. Defaults to
        ``{'status': ['new', 'open']}``.
    """
    if params is None:
        params = {'status': ['new', 'open']}
    issues = []
    if name is not None:
        try:
            issues = parse_to_object(name).get_issues(**params)
        except (KeyError, AttributeError):
            raise ObjectNotFound(name, 'repository')
    else:
        buckets = get_bitbucket_remotes()
        if not buckets:
            raise CommandError('No BitBucket remotes to use.')
        for name in buckets:
            try:
                issues += buckets[name].get_issues(**params)
            except BitBucketAPIError as e:
                logger.warning(e)
    return issues


def show(name=None):
    """Describe an API object.

    :param name: The name pointing to an API object. If ommited or given
        ``None``, will try to describe the default remote.
    """
    obj = parse_to_object(name)
    return obj.get_report()


def open(name=None):
    """Open the BitBucket page of an API object.

    :param name: The name pointing to an API object. If ommited or given
        ``None``, will try to open the default remote.
    """
    obj = parse_to_object(name)
    webbrowser.open(obj.get_webpage_url())
