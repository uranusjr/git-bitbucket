#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from . import actions


def issues_cmd(name=None, params=None):
    """Prints issues in one or all BitBucket repositories.

    options:
        -n=<str>, --name=<str>
            Remote name pointing to a BitBucket repository.
        --params=<json>
            Parameters to query the BitBucket API.
    """
    issues = actions.issues(name, params)
    if not issues:
        return 'No issues'
    outputs = []
    for issue in issues:
        issue_string = '{repo}\t{id}\t{title}'.format(
            repo=issue.repo.name,
            id='#{id}'.format(id=issue.id).rjust(5),
            title=issue.title
        )
        outputs.append(issue_string)
    return '\n'.join(outputs)


def show_cmd(name=None):
    """Show description of a BitBucket API object.

    options:
        -n=<str>, --name=<str>
            Description of a BitBucket API object.
    """
    return actions.show(name)


def open_cmd(name=None):
    """Open the BitBucket page of a BitBucket API object.

    options:
        -n=<str>, --name=<str>
            Description of a BitBucket API object.
    """
    actions.open(name)
