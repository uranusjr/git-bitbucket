#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import subprocess
import sys
import requests


def git(command, *params):
    args = ('git', command,) + params
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    return (
        stdout.decode(sys.stdout.encoding),
        stderr.decode(sys.stderr.encoding),
    )


def get_auth():
    username = git('config', 'bitbucket.username')[0].strip()
    password = git('config', 'bitbucket.password')[0].strip()
    if username and password:
        return requests.auth.HTTPBasicAuth(username, password)
    return None


def urljoin(*components):
    if not components:
        raise IndexError('urljoin requires at least one component to join.')
    prefix = components[0].rstrip('/')
    components = [c.strip('/') for c in components[1:] if c is not None]
    components.insert(0, prefix)
    url = '/'.join(components)
    if not url.endswith('/'):
        url = url + '/'
    return url
