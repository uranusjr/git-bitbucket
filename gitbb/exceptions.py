#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals


class CommandError(Exception):
    pass


class BitBucketAPIError(Exception):
    pass


class ObjectNotFound(CommandError):
    def __init__(self, name, key='object'):
        msg = '{key} not found: {name}'.format(key=key.title(), name=name)
        super(ObjectNotFound, self).__init__(msg)
