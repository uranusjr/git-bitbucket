#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import requests
from .utils import get_auth, urljoin
from .exceptions import BitBucketAPIError


class BitBucket(object):
    """Common representation of a BitBucket API object.
    """
    def get_webpage_url(self):
        return 'https://bitbucket.org/'

    def get_api_prefix_1(self):
        return 'https://bitbucket.org/api/1.0/'

    def get_api_url(self, endpoint=None):
        return urljoin(self.get_api_prefix_1(), endpoint)

    def get_api_request(self, endpoint=None, params=None, auth=None):
        if auth is None:
            auth = get_auth()
        url = self.get_api_url(endpoint)
        r = requests.get(url, params=params, auth=auth)
        if r.status_code != 200:
            raise BitBucketAPIError(
                'Could not fetch issues for {obj}. {code}.'.format(
                    obj=self, code=r.status_code,
                )
            )
        return r


class Repository(BitBucket):
    """Representation of a BitBucket repository.
    """
    ENDPOINT_ISSUES = 'issues'

    def __init__(self, account, repo, name=None):
        super(Repository, self).__init__()
        if name is None:
            name = '{account}/{repo}'.format(account=account, repo=repo)
        self.name = name
        self.account = account
        self.repo = repo

    def __str__(self):
        return 'repository {account}/{repo} ({name})'.format(
            name=self.name, account=self.account, repo=self.repo,
        )

    def get_webpage_url(self):
        return urljoin(
            super(Repository, self).get_webpage_url(),
            self.account, self.repo,
        )

    def get_api_prefix_1(self):
        return urljoin(
            super(Repository, self).get_api_prefix_1(), 'repositories',
        )

    def get_api_url(self, endpoint=None):
        endpoint = endpoint or ''
        url = urljoin(
            self.get_api_prefix_1(), self.account, self.repo, endpoint,
        )
        return url

    def get_issues(self, **params):
        r = self.get_api_request(Repository.ENDPOINT_ISSUES, params=params)
        issues = []
        try:
            issue_dataset = r.json()['issues']
        except (ValueError, KeyError):
            return issues
        for data in issue_dataset:
            issue = Issue(repo=self, id=data['local_id'], title=data['title'])
            issues.append(issue)
        return issues

    def get_report(self):
        data = self.get_api_request().json()
        return '{scm} repository {account}/{repo}'.format(
            scm=data['scm'].title(), account=self.account, repo=self.repo,
        )


class Issue(BitBucket):
    """Representation of a BitBucket issue.
    """
    def __init__(self, repo, id, title=None):
        super(Issue, self).__init__()
        self.repo = repo
        self.id = id
        self.title = title

    def __str__(self):
        return 'issue #{id} in {repo}'.format(id=self.id, repo=self.repo.name)

    def get_webpage_url(self):
        return urljoin(self.repo.get_webpage_url(), 'issue', self.id)

    def get_api_prefix_1(self):
        return urljoin(self.repo.get_api_url(), 'issues')

    def get_api_url(self, endpoint=None):
        return urljoin(self.get_api_prefix_1(), self.id, endpoint)

    def get_report(self):
        data = self.get_api_request().json()

        fmt = (
            '\n'
            '#{id} {title} ({status} {kind})\n'
            '\n'
            '{content}\n'
        )
        return fmt.format(**{
            'id': data['local_id'], 'title': data['title'],
            'kind': data['metadata']['kind'], 'status': data['status'],
            'content': data['content'],
        })
