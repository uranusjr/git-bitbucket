#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from collections import OrderedDict
import re
from .utils import git
from .bitbucket import Repository, Issue
from .exceptions import ObjectNotFound, CommandError


BITBUCKET_URL_PATTERN_HTTPS = re.compile(
    r'^https://'
    r'(?:\w+@)?'            # Possible login name.
    r'bitbucket\.org/'
    r'(?P<account>\w+)/'
    r'(?P<repo>[\w\.\-]+)'
    r'(?:\.git)?$'          # May or may not end with ".git".
)

BITBUCKET_URL_PATTERN_SSH = re.compile(
    r'git@bitbucket\.org:(?P<account>\w+)/(?P<repo>[\w\.\-]+)\.git'
)

BITBUCKET_OBJECT_NAME_PATTERN = re.compile(
    r'''^
        (?:
            (?:(?P<account>\w+)/(?P<repo>[\w\.\-]+))
            |
            (?P<remote>[\w/]+)
        )?
        (?:\#(?P<issue_id>\d+))?$
    ''',
    re.VERBOSE
)


def get_bitbucket_remotes():
    """List BitBucket remotes of current repository.

    Parses the result of ``git remote -v`` of the current repository into a
    ordered dict, with the remote names as keys and ``Repository`` instances as
    values. Returns an empty ordered dict if no BitBucket remotes are found.
    """
    buckets = OrderedDict()
    for line in git('remote', '-v')[0].split('\n'):
        try:
            name, url = line.split()[:2]
        except ValueError:
            continue
        match = (
            BITBUCKET_URL_PATTERN_HTTPS.match(url)
            or BITBUCKET_URL_PATTERN_SSH.match(url)
        )
        if not match:   # Not a BitBucket remote. Ignore.
            continue
        # Push URLs always seem to come after fetch ones, so simple overriding
        # should be enough here? Need to confirm this.
        groups = match.groupdict()
        buckets[name] = Repository(
            account=groups['account'], repo=groups['repo'], name=name,
        )
    return buckets


def find_default_remote():
    """Get the "default remote" of current repository.

    This function returns the current repository's "default remote" based on
    the following logic:

    1. The remote named "origin".
    2. The first remote available.

    If a chosen remote has different push and pull URLs, the pull URL takes
    precedence. Raises a ``CommandError`` if no suitable remotes can be found.
    """
    remotes = get_bitbucket_remotes()
    if 'origin' in remotes:
        remote = remotes['origin']
    else:
        try:
            remote = remotes.values()[0]
        except IndexError:
            raise CommandError('No BitBucket remotes to use.')
    return remote


def parse_to_object(name):
    """Parse a given name into a BitBucket object.

    Possible patterns:
        None        (Returns *origin* or the first remote.)
        #<issue>    (Returns issue from *origin* or the first remote.)
        <remote>
        <remote>#<issue>
        <account>/<repository>
        <account>/<repository>#<issue>

    Remote names will be verified with information from the local repository,
    but other entries will not.
    """
    if name is None:
        groups = {}
        bucket = find_default_remote()
    else:
        match = BITBUCKET_OBJECT_NAME_PATTERN.match(name)
        if match is None:
            raise ObjectNotFound(name)
        groups = match.groupdict()
        remote = groups.get('remote')
        account = groups.get('account')
        repo = groups.get('repo')
        if remote:
            buckets = get_bitbucket_remotes()
            try:
                bucket = buckets[remote]
            except KeyError:
                raise ObjectNotFound(remote, 'repository')
        elif account and repo:
            bucket = Repository(account=account, repo=repo)
        else:
            bucket = find_default_remote()
    issue_id = groups.get('issue_id')
    if issue_id:
        return Issue(bucket, issue_id)
    return bucket
