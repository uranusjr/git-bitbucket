==============
Git-Bitbucket
==============

Git-Bitbucket is a custom Git subcommand to interact with BitBucket remotes
through its REST API.


Installation and Usage
=======================

Use ``pip``. This installs the ``gitbb`` module and a ``git-bitbucket``
executable. You can then use it as a Git subcommand, e.g.::

    git bitbucket help


Available Commands
===================

Issues
-------

Syntax: ``git bitbucket issues [--name=<remote_name>] [--params=<params>]``

List issues in a given BitBucket remote (or all available remotes if none
specified). Optional filtering/ordering parameters can be specified through
the ``params`` option.

This has a shorthand: ``git bitbucket``.


Show
-----

Syntax: ``git bitbucket show [--name=<object_name>]``

Describe a given remote, or a given issue in a remote. If no remotes are
specified, this will attempt to describe the default remote (see later section
for how this is determined).

``object_name`` is the name of the API object. See later section to find out
what this is.



Open
-----

Syntax: ``git bitbucket open [--name=<object_name>]``

Open the BitBucket page of a given remote, or a given issue in a remote. If no
remotes are specified, this will attempt to open the default remote (see later
section for how this is determined).

``object_name`` is the name of the API object. See later section to find out
what this is.


The “Default Remote”
----------------------

Under various circumstances, you can omit the name of an API object completely,
and Git-Bitbucket will automatically find the “default” remote for you
according to context. The logic is:

1. If your ``origin`` remote is a BitBucket repository, it will become your
   default remote.
2. If any of your remotes is a BitBucket repository, one of them will become
   your default remote, based on the order of appereance in ``git remote``.
   If the remote chosen has different push and pull URLs, the push URL takes
   precedence.
3. If you do not have any BitBucket remotes, an error is generated.


API Object Description Syntax
------------------------------

For any commands that accepts a “name of an API object,” you can use one of the
following ways to represent an API object you want to specify:

1. ``<remote_name>``. Open repository page corresponding to the remote.
2. ``<account>/<repository>``. Open repository page of the account.
3. ``#<issue_number>``. Open issue page in the default remote.
4. ``<remote_name>#<issue_number>``. Open issue page of repository
   corresponding to the remote.
5. ``<account>/<repository>#<issue_number>``. Open issue page of repository.

Note that if your shell uses ``#`` for comments (most shells do), you will need
to escape the character in form 3::

    git bitbucket open \#3      # This is a comment.
    git bitbucket open "#3"     # This is equivalent to the above.

If you don’t specify and API object when you should, the “default remote”
(described above) will be used under most circumstances if this makes sense.


License
========

BSD 3-clause.
