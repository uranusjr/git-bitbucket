#!/usr/bin/env python
# -*- coding: utf-8 -*-

from importlib import import_module
from setuptools import setup, find_packages


version = import_module('gitbb').VERSION
readme = open('README.rst').read()

requirements = (
    'clime',
    'requests',
)

test_requirements = (
    # TODO: put package test requirements here
)

setup(
    name='git-bitbucket',
    version=version,
    description='BitBucket API for Git.',
    long_description=readme,
    author='Tzu-ping Chung',
    author_email='uranusjr@gmail.com',
    packages=find_packages(),
    scripts=('bin/git-bitbucket',),
    include_package_data=True,
    install_requires=requirements,
    license='BSD',
    zip_safe=False,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
    ],
    test_suite='tests',
    tests_require=test_requirements
)
